#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define AMOUNT_OF_STEPS 200000000

static int threads = -1;

void* body(void* param){
	long double *res = (long double*)malloc(sizeof(long double));
	int num = (int)param;
	//printf("%d:%d\n", num, 101);
	int i, start = AMOUNT_OF_STEPS/threads * num, border;

	if(num == 0)
		start = 0;
	else
		start++;
	if(num + 1 == threads)
		border = AMOUNT_OF_STEPS;
	else
		border = AMOUNT_OF_STEPS/threads * (num + 1);
	//printf("%d:%d-%d\n", num, start, border);
	*res = 0.0;
	for (i = start; i < border; i++){
		*res += 1.0/(i*4.0 + 1.0);
		*res -= 1.0/(i*4.0 + 3.0);
	}
	//printf("%d:%Lf\n", num, *res);
	pthread_exit(res);
}

int main(int argc, char** argv){
	if (argc > 1)
		threads = atoi(argv[1]);
	else{
		printf("Second argument is absent.\n");
		exit(1);
	}
	int i;
	//printf("threads = %d\n", threads);
	pthread_t *pthreads = (pthread_t*)malloc(sizeof(pthread_t) * threads);
	
	for(i = 0; i < threads; i++){
		int errorCode = pthread_create(pthreads + i, NULL, body, (void*)i);
		if (errorCode){
			printf("Error occured creating thread.\n");
			exit(2);
		}
	}
	long double pi = 0;
	for(i = 0; i < threads; i++){
		long double *res;
		pthread_join(*(pthreads + i), (void**)&res);
		pi += *res;
	}
	pi *= 4;
	printf("PI = %.10Lf\n", pi);
	pthread_exit(0);
	return 0;
}

