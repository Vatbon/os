#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>


void * body(void *param){
	char **temp = (char**)param;
	while (*temp != NULL){
		printf("%s\n", *temp);
		temp++;
	}
	return 0;
}

int main(){
	pthread_t thread[4];
	int errorCode;
	static char *param_1[]={ "1.1", "1.2", "1.3", "1.4", NULL };
	static char *param_2[]={ "2.1", "2.2", "2.3", NULL };
	static char *param_3[]={ "3.1", "3.2", "3.3", "3.4", NULL };
	static char *param_4[]={ "4.1", "4.2", "4.3", NULL };

	errorCode=pthread_create(&thread[0], NULL, body, param_1);
	if (errorCode != 0) {
		fprintf(stderr, "Unable to create thread");
		exit(1);
	}
	errorCode=pthread_create(&thread[1], NULL, body, param_2);
	if (errorCode != 0) {
		fprintf(stderr, "Unable to create thread");
		exit(1);
	}	
	errorCode=pthread_create(&thread[2], NULL, body, param_3);
	if (errorCode != 0) {
		fprintf(stderr, "Unable to create thread");
		exit(1);
	}
	errorCode=pthread_create(&thread[3], NULL, body, param_4);
	if (errorCode != 0) {
		fprintf(stderr, "Unable to create thread");
		exit(1);
	}
	pthread_exit(NULL);
	return 0;
}
