#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void* body(){
	while(1){
		write(1, "I'm alive\n", 11);
		usleep(100000); /*0.1 sec*/
	}
	return 0;
}

int main(){
	pthread_t thread;
	int errorCode;
	errorCode = pthread_create(&thread, NULL, body, NULL);
	if (errorCode != 0) {
		fprintf(stderr, "Unable to create thread");
		exit(1);
	}
	sleep(2);
	pthread_cancel(thread);
	pthread_exit(0);
	return 0;
}

