#include <stdio.h>
#include <pthread.h>

char* mom = "I'm a mother\n";
char* child = "I'm a child\n";

void* thr(){
	int i;
	for (i = 0; i < 10; i++)
		write(1, child, 12);
	return 0;
}

int main(){
	pthread_t thread;
	int error = pthread_create(&thread, NULL, thr, NULL);
	pthread_join(thread, NULL);
	int i;
	for (i = 0; i < 10; i++)
		write(1, mom, 13);
	pthread_exit(NULL);
	return 0;
}
