#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	unsigned long shift[500];
	int fd, i, 
	    j, 
	    line_num, 
	    line_len[500],
	    r;	  
	char c, buf[500];

	if (argc < 2){
		printf("Missing argument.\n");
		exit(1);
	} 
	fd = open(argv[1], O_RDONLY);
	if (fd == -1) {
		perror(argv[1]);
		exit(1);
	}

	shift[1] = 0L;
	i = 1;
	j = 0;
	while (r = read(fd, &c, 1)){
		if (r == -1){
			perror("read(2)");
			close(fd);
			exit(1);
		}
		if(c == '\n') {
			j++;
			line_len[i] = j;
			i++;
			shift[i] = lseek(fd, 0L, 1);
			if (shift[i] == -1){
				perror("lseek(2)");
				close(fd);
				exit(1);
			}
			j = 0;
		}
		else {
			j++;
		}
	}
	while (printf("Line number : ") && scanf("%d", &line_num)) {
		if(line_num <= 0){
			close(fd);
			exit(0);
		}
		if (line_num < i){
			if(lseek(fd, shift[line_num], 0) == -1){
				perror("lseek(2)");
				close(fd);
				exit(1);
			}
			if(read(fd, buf, line_len[line_num]) == -1){
				perror("read(2)");
				close(fd);
				exit(1);
			}
			else{
				write(1, buf, line_len[line_num]);
				//lseek(fd, 0L, SEEK_END);
				//write(fd, buf, line_len[line_num]);
			}
		}
		else
			fprintf(stderr, "Wrong line number.\n");

	}
	close(fd);
	exit(0);
}
