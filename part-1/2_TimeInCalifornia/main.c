#define _GNU_SOURCE

#include <sys/types.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
extern char *tzname[];
int main() {
	time_t t; 
	time(&t);
	struct tm *sp;
	sp = localtime(&t);
	//tzset();
	t = t - sp->tm_gmtoff - 8*60*60 ;
	printf("Offset to GMT is %lds.\n", sp->tm_gmtoff);
	printf("UNIXTIME : %ld\n", t);
	printf("Time in LA (not changed TZ) : %s", ctime(&t));

	putenv("TZ=:America/Los_Angeles");
	time(&t);
	sp = localtime(&t);
	printf("Time in LA (modified TZ): %d/%d/%02d %d:%02d:%02d %s\n",
			sp->tm_mon + 1, sp->tm_mday, 1900 + sp->tm_year,
			sp->tm_hour, sp->tm_min, sp->tm_sec, tzname[sp->tm_isdst]);

	exit(0);
}
