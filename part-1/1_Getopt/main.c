#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <ulimit.h>

extern char **environ;

int main(int argc, char *argv[]){
	char options[ ] = "ispucdvU:C:V:";
	int c, invalid = 0;

	uid_t uid = 0;
	uid_t euid = 0;

	gid_t gid = 0;
	gid_t egid = 0;

	pid_t pid = 0;
	pid_t ppid = 0;
	pid_t gpid = 0;

	long int ulim = -1;

	struct rlimit rlim, crlim;
		char isRlim = 0;

	char *cwd = NULL;
	char *C_ptr, *U_ptr;

	int i;
	//printf("argc equals %d\n", argc);
	while ((c = getopt(argc, argv, options)) != EOF) {
		switch (c) {
			case 'V':// new var=data into environ
				putenv(optarg);
				break;
			case 'U':// new ulimit
				U_ptr = optarg;
				ulimit(UL_SETFSIZE, atol(U_ptr));
				break;
			case 'C'://new core size
				C_ptr = optarg;
				getrlimit(RLIMIT_CORE, &crlim);
				crlim.rlim_cur = atol(C_ptr);
				if (setrlimit(RLIMIT_CORE, &crlim) == -1)
					printf("Unable to change core size\n");
				break;
			case 'i':// real and eff uid
				uid = getuid();
				euid = geteuid();
				gid = getgid();
				egid = getegid();
				printf("uid = %d\neuid = %d\ngid = %d\negid = %d\n", uid, euid, gid, egid);
				break;
			case 's':// become a group leader
				if (setpgid(getpid(), getpid()) == -1)
					printf("Unable to become a group leader\n");
				break;
			case 'p':// pid ppid pgid
				pid = getpid();
				ppid = getppid();
				gpid = getpgid(0);
				printf("pid = %d\nppid = %d\ngpid = %d\n", pid, ppid, gpid);
				break;
			case 'u':// print ulimit
				ulim = ulimit(UL_GETFSIZE, 1024);
				printf("ulimit = %ld\n", ulim);
				break;
			case 'c':// print core size
				getrlimit(RLIMIT_CORE, &rlim);
				printf("core size = %ld\n", rlim.rlim_cur);
				break;
			case 'd':// current working directory
				cwd = getcwd(NULL, 256);
				printf("cwd = %s\n", cwd);
				break;
			case 'v':// print env
				i = 0;
				while (environ[i] != NULL)
					printf("%s\n", environ[i++]);
				break;
			case '?':
				printf("invalid option is %c\n", optopt);
				invalid++;
		}
	}
	if(optind < argc)
		printf("next parameter = %s\n", argv[optind]);
}
