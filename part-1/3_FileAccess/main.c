#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc,const char** argv){
	if (argc < 2){
		printf("Please, enter file.\n");
		exit(1);
	}
	FILE *file;
	uid_t uid = getuid();
	printf("UID = %d\nEUID = %d\n", getuid(), geteuid());
	/*FIRST OPENING*/
	file = fopen(argv[1], "r+");
	if (file == NULL){
		perror(argv[1]);
	} 
	else{
		printf("Successfully opened %s file.\n", argv[1]);
	}
	if (file != NULL)
		if (fclose(file) == EOF){
			perror(argv[1]);
			exit(1);
		} 
		else{
			printf("Successfully closed %s file.\n", argv[1]);
		}
	/*SETTING EUID*/
	if(setuid(uid = getuid()) == -1){
		perror("Unable to set EUID");
		exit(1);
	} 
	else{
		printf("Set to %d\n", uid);
		printf("UID = %d\nEUID = %d\n", getuid(), geteuid());
	}
	/*SECOND OPENING*/
	file = fopen("./samplefile", "r+");
	if (file == NULL){
		perror(argv[1]);
	} 
	else{
		printf("Successfully opened %s file.\n", argv[1]);
	}
	if (file != NULL)
		if (fclose(file) == EOF){
			perror(argv[1]);
			exit(1);
		} 
		else{
			printf("Successfully closed %s file.\n", argv[1]);
		}
	/*EXIT ON SUCCESS*/
	exit(0);
}
