#include <sys/types.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

unsigned int i,r,line_len[500],shift[500],fd;
char buf[BUFSIZ];

void handler(int sig){
	for (r = 1; r < i; r++){
		if(lseek(fd, shift[r], 0) == -1){
			perror("lseek(2)");
			close(fd);
			exit(1);
		}
		if(read(fd, buf, line_len[r]) == -1){
			perror("read(2)");
			close(fd);
			exit(1);
		}
		else
			write(1, buf, line_len[r]);
	}
	close(fd);
	exit(0);
}


int main(int argc, char *argv[])
{
	struct sigaction actalrm;
	int j, 
	    line_num; 
	char c;
	if (argc < 2){
		printf("Missing argument.\n");
		exit(1);
	} 
	fd = open(argv[1], O_RDONLY);
	if (fd == -1) {
		perror(argv[1]);
		exit(1);
	}

	shift[1] = 0L;
	i = 1;
	j = 0;
	while (r = read(fd, &c, 1)){
		if (r == -1){
			perror("read(2)");
			close(fd);
			exit(1);
		}
		if(c == '\n') {
			j++;
			line_len[i] = j;
			i++;
			shift[i] = lseek(fd, 0L, 1);
			if (shift[i] == -1){
				perror("lseek(2)");
				close(fd);
				exit(1);
			}
			j = 0;
		}
		else {
			j++;
		}
	}
	actalrm.sa_handler = &handler;
	sigaction(SIGALRM, &actalrm, NULL);
	printf("Line number: ");
	alarm(5);
	while (scanf("%d", &line_num)) {
		if(line_num <= 0){
			close(fd);
			exit(0);
		}
		if (line_num < i){
			if(lseek(fd, shift[line_num], 0) == -1){
				perror("lseek(2)");
				close(fd);
				exit(1);
			}
			if(read(fd, buf, line_len[line_num]) == -1){
				perror("read(2)");
				close(fd);
				exit(1);
			}
			else
				write(1, buf, line_len[line_num]);
		}
		else
			fprintf(stderr, "Wrong line number.\n");
		alarm(5);
		printf("Line number: ");
	}
	close(fd);
	exit(0);
}
