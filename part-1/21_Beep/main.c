
#include <signal.h>
#include <stdlib.h>

int rings;

void sigint(int);
void sigquit(int);

int main()
{
	rings = 0;
	struct sigaction acti;
	struct sigaction actq;
	acti.sa_handler = &sigint;
	actq.sa_handler = &sigquit;
	sigaction(SIGINT, &acti, NULL);
	sigaction(SIGQUIT, &actq, NULL);
	while (1){;}
	exit(0);
}

void sigint(int sig){
	rings++;
	write(1, "\07", 1);
}

void sigquit(int sig){
	char c[16];
	unsigned char i = 15;
	do{
		c[i--] = rings % 10 + '0';
		rings /= 10;
	}while(rings);
	write(1, c + i + 1, 16 - i - 1);
	write(1,"\n", 1);
	exit(0);
}


