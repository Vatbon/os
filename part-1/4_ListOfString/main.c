#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

typedef struct node {
	char *data;
	struct node *next;
} linked_node;

linked_node *push(linked_node *head,char *data){
	linked_node *curr = head;
	while (curr->next != NULL){
		curr = curr->next;
	}
	curr = curr->next = (linked_node*)malloc(sizeof(linked_node));
	curr->data = (char*)malloc(strlen(data) + 1);
	strcpy(curr->data, data);
	curr->next = NULL;
	return head;
}

void print_list(linked_node *head){
	linked_node *curr = head->next;
	int i= 0;
	while (curr != NULL){
		printf("%d: %s\n", ++i, curr->data);
		curr = curr->next;
	}
}

linked_node *create_list(){
	linked_node *curr = (linked_node*)malloc(sizeof(linked_node));
	curr->data = "";
	curr->next = NULL;
	return curr;
}

int main(){
	linked_node* list = create_list();
	char str[BUFSIZ];
	printf("Enter lines of text:\n");
	while(gets(str) != NULL){
		if(str[0] == '.')
			break;
		push(list, str);
	};
	print_list(list);
	exit(0);
}

