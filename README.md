![logoSolaris](images/Sol10logo.png)![logoNSU](images/NsuLogoRus.png)

# OS
Данный репозиторий содержит лабораторный работы, выполненные [Неволином Владимиром](https://vk.com/vatbon).
* [Git-репозиторий Illumos-gate](https://github.com/illumos/illumos-gate)

## Первая часть курса OS
Директория `part-1` содержит лабораторные задания связанные с данным курсом.
* [Главная страница fat](http://ccfit.nsu.ru/~fat/)
* [Методичка и презентации](http://parallels.nsu.ru/~fat/unixsvr4-new/trunk/)
* [Тексты заданий (RUS)](http://ccfit.nsu.ru/~deviv/courses/unix/tasks.html)
* [Тексты заданий (ENG)](http://ccfit.nsu.ru/~fat/tasks-eng.html)
* [Лекции в формате pdf](https://nsu.ru/xmlui/handle/nsu/980)

## Вторая часть курса OS
Директория `part-2` содержит лабораторные задания связанные с данным курсом.
 * [Главная страница практикума](http://parallels.nsu.ru/WackoWiki/KursOperacionnyeSistemy/PraktikumPosixThreads?v=77v)
 * [Задания практикума](http://parallels.nsu.ru/WackoWiki/KursOperacionnyeSistemy/PraktikumPosixThreads/PthreadTasks?v=1bu5)
 * [Книги для курса POSIX Threads](https://drive.google.com/drive/folders/1m5xVCCoz1vzTufXadynFCEOaP2RJ1H4X)

### Примечание
Адрес сервера НГУ: `84.237.52.20`\
Адрес кластера Solaris(виден из сервера НГУ): `solarka.ccfit.nsu.ru`
